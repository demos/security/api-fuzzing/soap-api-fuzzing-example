soap-api-fuzzing-example

This project is an example of API Fuzzing testing a SOAP web service. The SOAP web service
provides the API operations to test. **NEVER** run fuzz testing against a production server. Not only can it perform any function that the API can, it may also trigger bugs in the API.
In this example, the testing is against a docker container created in a stage prior the fuzz stage. 
The testing results are available via the Test tab of the pipeline.