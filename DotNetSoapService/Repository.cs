﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DotNetSoapService.Services;
using Microsoft.Data.Sqlite;

namespace DotNetSoapService
{
    public static class Repository
    {
        private static readonly string ConnectionString;

        private static readonly object _lock = new object();

        static Repository()
        {
            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var db = Path.Combine(dir, "datastore.sqlite");
            if (!File.Exists(db))
                CreateFile(db);

            ConnectionString = $"Data Source={db}";

            using var conn = GetConnection();
            var sql = "DROP TABLE IF EXISTS users";
            using (var cmd = new SqliteCommand(sql, conn))
                cmd.ExecuteNonQuery();

            sql = "CREATE TABLE users (user_id INTEGER PRIMARY KEY AUTOINCREMENT, user TEXT UNIQUE, first TEXT, last TEXT, password TEXT)";
            using (var cmd = new SqliteCommand(sql, conn))
                cmd.ExecuteNonQuery();

            sql = "INSERT INTO users (user, first, last, password) VALUES ('admin', 'Joe', 'Smith', 'Password!')";
            using (var cmd = new SqliteCommand(sql, conn))
                cmd.ExecuteNonQuery();

            long userId;
            using (var cmd = new SqliteCommand("SELECT last_insert_rowid()", conn))
                userId = (long)cmd.ExecuteScalar();

            sql = "DROP TABLE IF EXISTS msgs";
            using (var cmd = new SqliteCommand(sql, conn))
                cmd.ExecuteNonQuery();

            sql = "CREATE TABLE msgs (msg_id INTEGER PRIMARY KEY AUTOINCREMENT, from_id INT, to_id INT, subject TEXT, msg TEXT)";
            using (var cmd = new SqliteCommand(sql, conn))
                cmd.ExecuteNonQuery();

            sql = $"INSERT INTO msgs (from_id, to_id, subject, msg) VALUES ('{userId}','{userId}', 'Hello From Myself', 'Welcome to the system...!')";
            using (var cmd = new SqliteCommand(sql, conn))
                cmd.ExecuteNonQuery();

        }

        /// <summary>
        /// Creates a database file.  This just creates a zero-byte file which SQLite
        /// will turn into a database when the file is opened properly.
        /// </summary>
        /// <param name="databaseFileName">The file to create</param>
        public static void CreateFile(string databaseFileName)
            => File.WriteAllBytes(databaseFileName, new byte[0]);

        public static SqliteConnection GetConnection()
        {
            var c = new SqliteConnection(ConnectionString);
            c.Open();
            return c;
        }

        public static Account[] GetAccounts()
        {
            lock (_lock)
            {
                using var conn = GetConnection();
                var sql = "SELECT user_id, user, first, last, password from users";
                using var cmd = new SqliteCommand(sql, conn);
                using var reader = cmd.ExecuteReader();
                var accounts = new List<Account>();

                while (reader.Read())
                    accounts.Add(new Account
                    {
                        UserId = reader.GetInt32(0),
                        User = reader.GetString(1),
                        First = reader.GetString(2),
                        Last = reader.GetString(3),
                        Password = reader.GetString(4)
                    });

                return accounts.ToArray();
            }
        }

        public static Account GetAccount(int userId)
        {
            lock (_lock)
            {
                using var conn = GetConnection();
                var sql = $"SELECT user_id, user, first, last, password FROM users WHERE user_id = {userId}";

                using var cmd = new SqliteCommand(sql, conn);
                var reader = cmd.ExecuteReader();

                if (!reader.Read())
                    return null;

                return new Account
                {
                    UserId = reader.GetInt32(0),
                    User = reader.GetString(1),
                    First = reader.GetString(2),
                    Last = reader.GetString(3),
                    Password = reader.GetString(4)
                };
            }
        }

        public static Account GetAccount(string user)
        {
            lock (_lock)
            {
                using var conn = GetConnection();
                var sql = $"SELECT user_id, user, first, last, password FROM users WHERE user = '{user}'";

                using var cmd = new SqliteCommand(sql, conn);
                var reader = cmd.ExecuteReader();

                if (!reader.Read())
                    return null;

                return new Account
                {
                    UserId = reader.GetInt32(0),
                    User = reader.GetString(1),
                    First = reader.GetString(2),
                    Last = reader.GetString(3),
                    Password = reader.GetString(4)
                };
            }
        }

        public static Account Create(Account account)
        {
            lock (_lock)
            {
                var sql = "INSERT INTO users (user, first, last, password) VALUES ( " +
                            $"'{account.User}', '{account.First}', '{account.Last}', '{account.Password}')";

                using var conn = GetConnection();
                using (var cmdInsert = new SqliteCommand(sql, conn))
                    cmdInsert.ExecuteNonQuery();
                using (var cmdLastInsertId = new SqliteCommand("SELECT last_insert_rowid()", conn))
                    account.UserId = (int)((long)cmdLastInsertId.ExecuteScalar());

                return account;
            }
        }

        public static bool Save(Account account)
        {
            lock (_lock)
            {
                using var conn = GetConnection();
                var sql = "UPDATE users " +
                          $"SET user = '{account.User}' " +
                          $", first = '{account.First}' " +
                          $", last = '{account.Last}' " +
                          $", password = '{account.Password}' " +
                          $"WHERE user_id = {account.UserId}";

                using var cmd = new SqliteCommand(sql, conn);
                return cmd.ExecuteNonQuery() == 1;
            }
        }

        public static void Delete(int userId)
        {
            lock (_lock)
            {
                using var conn = GetConnection();
                var sql = $"DELETE FROM users WHERE user_id = {userId}";
                using var cmd = new SqliteCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteAll()
        {
            lock (_lock)
            {
                using var conn = GetConnection();
                var sql = "DELETE FROM users";
                using var cmd = new SqliteCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
        }
    }
}